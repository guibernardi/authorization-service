package exchange.monkey.authorization.domain;

public interface TokenService {
    Token save(Long pessoaId, String email, String strToken);
    //Token generateToken(String email, Long pessoaId);
    Token validateToken(String token);
}
