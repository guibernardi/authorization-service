package exchange.monkey.authorization.api.v1;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

@Data
@EqualsAndHashCode(callSuper = true)
@Relation(value = "token", collectionRelation = "tokens")
class TokenResource extends ResourceSupport {
    private Long pessoaId;
    private String email;
    private String token;
}

