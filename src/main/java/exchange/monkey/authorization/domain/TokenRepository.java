package exchange.monkey.authorization.domain;


import org.springframework.data.jpa.repository.JpaRepository;

interface TokenRepository extends JpaRepository<Token, Long> {
    Token findByToken(String token);
    Token findTokenByPessoaIdAndAndEmail(Long pessoaId, String email);
}
