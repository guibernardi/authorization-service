package exchange.monkey.authorization.integration;

import lombok.Data;

@Data
public class BackofficeResponse {
    private String nome;
    private String email;
    private String telefone;
}
