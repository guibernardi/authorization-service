package exchange.monkey.authorization.api.v1;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import exchange.monkey.authorization.domain.Token;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class TokenResourceAssembler extends ResourceAssemblerSupport<Token, TokenResource> {

    public TokenResourceAssembler() {
        super(AuthorizationRestService.class, TokenResource.class);
    }

    @Override
    public TokenResource toResource(Token token) {
        TokenResource tokenResource = createResourceWithId(token.getId(), token);
        tokenResource.setEmail(token.getEmail());
        tokenResource.setPessoaId(token.getPessoaId());
        tokenResource.setToken(token.getToken());
        addLinks(tokenResource);
        return tokenResource;
    }

    public Token toDomain(TokenResource tokenResource) {
        return Token.builder()
                .pessoaId(tokenResource.getPessoaId())
                .email(tokenResource.getEmail())
                .token(tokenResource.getToken())
                .build();
    }

    private void addLinks(TokenResource tokenResource) {
        tokenResource.add(linkTo(methodOn(AuthorizationRestService.class).validateToken(tokenResource.getToken())).withRel("validate"));
    }
}

