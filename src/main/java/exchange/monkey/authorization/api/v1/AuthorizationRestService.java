package exchange.monkey.authorization.api.v1;

import exchange.monkey.authorization.domain.TokenService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/v1/authorization")
public class AuthorizationRestService {

    private final TokenService tokenService;
    private final TokenResourceAssembler tokenResourceAssembler;

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{pessoaId}/{email}/{strToken}", method = RequestMethod.POST)
    public TokenResource save(@PathVariable(name = "pessoaId") Long pessoaId, @PathVariable(name = "email") String email,
                              @PathVariable(name = "strToken") String strToken) {
        return tokenResourceAssembler.toResource(tokenService.save(pessoaId, email, strToken));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{token}/validate", method = RequestMethod.GET)
    public TokenResource validateToken(@PathVariable(name = "token") String token) {
        return tokenResourceAssembler.toResource(tokenService.validateToken(token));
    }
}
