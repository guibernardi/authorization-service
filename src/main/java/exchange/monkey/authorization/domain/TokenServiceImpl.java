package exchange.monkey.authorization.domain;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import exchange.monkey.authorization.integration.BackofficeClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
class TokenServiceImpl implements TokenService {

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private BackofficeClient backofficeClient;

    @Override
    public Token save(Long pessoaId, String email, String strToken) {
        Token t = tokenRepository.findTokenByPessoaIdAndAndEmail(pessoaId, email);

        if (t != null) {
            if (t.getValidatedAt() != null) {
                throw new RuntimeException(String.format("Token já confirmado em %s.",
                        t.getValidatedAt()));
            } else {
                t.setToken(strToken);
            }
        } else {
            t = new Token();
            t.setPessoaId(pessoaId);
            t.setEmail(email);
            t.setToken(strToken);
        }

        return tokenRepository.save(t);
    }

    //@HystrixCommand(fallbackMethod = "validateTokenFallback")
    @Override
    public Token validateToken(String strToken) {
        if (!jwtTokenProvider.validateToken(strToken)) {
            throw new RuntimeException("Token inválido. Favor verificar");
        }

        Token token = tokenRepository.findTokenByPessoaIdAndAndEmail(jwtTokenProvider.getPessoaIdFromJWT(strToken),
                jwtTokenProvider.getUserEmailFromJWT(strToken));

        if (token != null) {
            if (token.getValidatedAt() != null) {
                throw new RuntimeException(String.format("Token já confirmado em %s.",
                        token.getValidatedAt()));
            }

            backofficeClient.confirm(token.getPessoaId());

            if (token.getToken().equals(strToken)) {
                token.setValidatedAt(LocalDateTime.now());
            }
        } else {
            throw new RuntimeException("Token não encontrado. Favor verificar");
        }

        return tokenRepository.save(token);
    }

    /*public Token validateTokenFallback(String strToken) {
        throw new RuntimeException("Ops! Serviço indisponível no momento. Tente novamente mais tarde.");
    }*/
}
