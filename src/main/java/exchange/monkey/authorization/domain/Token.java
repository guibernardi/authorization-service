package exchange.monkey.authorization.domain;

import exchange.monkey.authorization.auditing.DateAudit;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "tokens")
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Token extends DateAudit implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Long id;

    @NotNull
    @Column(name = "pessoa_id")
    private Long pessoaId;

    @NotNull
    @Column(name = "email")
    private String email;

    @NotNull
    @Column(name = "token")
    private String token;

    @Column(name = "validated_at")
    private LocalDateTime validatedAt;
}
