package exchange.monkey.authorization.integration;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "backoffice",  url = "http://localhost:8080/api/backoffice/v1/pessoas") //url = "http://localhost:8081/v1/pessoas")
public interface BackofficeClient {

    @RequestMapping(value = {"{id}/confirm"}, method = RequestMethod.POST)
    BackofficeResponse confirm(@PathVariable("id") Long id);
}
